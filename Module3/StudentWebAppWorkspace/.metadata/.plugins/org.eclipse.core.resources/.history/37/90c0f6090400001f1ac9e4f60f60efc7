package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDao {

	public Student studLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from Student where emailId = ? and password = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Student stud = new Student();
				
				stud.setstudId(rs.getInt(1));
				stud.setstudName(rs.getString(2));
				stud.setGender(rs.getString(4));
				stud.setEmailId(rs.getString(5));
				stud.setPassword(rs.getString(6));
				
				return stud;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public int studRegister(Student stud) {
		PreparedStatement pst = null;
		Connection con = DbConnection.getConnection();
		
		String insertQry = "insert into Student " + 
		"(studName, gender, emailId, password) values (?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, stud.getstudName());
			pst.setString(3, stud.getGender());
			pst.setString(4, stud.getEmailId());
			pst.setString(5, stud.getPassword());
			
			return pst.executeUpdate();	//returns 1 or 0
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

	public Student getStudentById(int studId) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from Student where studId = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setInt(1, studId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Student stud = new Student();
				
				stud.setstudId(rs.getInt(1));
				stud.setstudName(rs.getString(2));
				stud.setGender(rs.getString(4));
				stud.setEmailId(rs.getString(5));
				stud.setPassword(rs.getString(6));
				
				return stud;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public List<Student> getAllStudents() {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from Student";
		
		try {
			pst = con.prepareStatement(selectQry);
			rs = pst.executeQuery();
			
			List<Student> studList = new ArrayList<Student>();
			
			while (rs.next()) {
				
				Student stud = new Student();
				
				stud.setstudId(rs.getInt(1));
				stud.setstudName(rs.getString(2));
				stud.setGender(rs.getString(4));
				stud.setEmailId(rs.getString(5));
				stud.setPassword(rs.getString(6));
				
				studList.add(stud);
			}
			
			return studList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
}
