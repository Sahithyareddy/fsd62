// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrl: './login.component.css'
// })
// export class LoginComponent implements OnInit{
//   email :string;
//   password :string;

//   constructor(){
//     this.email='';
//     this.password='';
//   }
  
//   ngOnInit() {
//   }

//   loginSubmit(loginForm: any) {
//     console.log(loginForm);

//     // if (loginForm.value.emailId == "HR" && loginForm.value.password == "HR") {
      

//     if (loginForm.emailId == "HR" && loginForm.password == "HR") {
//       alert('Login Success');
//     } else {
//       alert("Invalid Credentials")
//     }

//   }
//   submit() {
//    if(this.email == this.password){
//     alert("login succesfull");
//    }
//    else{
//     alert("fail");
//    }
//   }

// }


import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{
  emp: any;
  emailId: any;
  password: any;
  employees: any;

  //Dependency Injection for EmpService and Router (RouterModule)
  constructor(private service: EmpService, private router: Router) {
    this.employees = [
      {empId:101, empName:'Harsha',  salary:1212.12, gender:'Male',   doj:'05-25-2018', country:"IND", emailId:'harsha@gmail.com',  password:'Manasa@27'},
      {empId:102, empName:'Pasha',   salary:2323.23, gender:'Male',   doj:'06-26-2017', country:"USA", emailId:'pasha@gmail.com',   password:'Manasa@27'},
      {empId:103, empName:'Indira',  salary:3434.34, gender:'Female', doj:'07-27-2016', country:"CHI", emailId:'indira@gmail.com',  password:'Manasa@27'},
      {empId:104, empName:'Vamshi',  salary:4545.45, gender:'Male',   doj:'08-28-2015', country:"JAP", emailId:'vamshi@gmail.com',  password:'Manasa@27'},
      {empId:105, empName:'Krishna', salary:5656.56, gender:'Male',   doj:'09-29-2014', country:"UK",  emailId:'krishna@gmail.com', password:'Manasa@27'}
    ];
  }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  loginSubmit(loginForm: any) {
    // console.log(loginForm);

    // if (loginForm.emailId == "HR" && loginForm.password == "HR") {
    //   this.service.setUserLoggedIn();
    //   this.router.navigate(['showemps']);
    // } else {
    //   alert("Invalid Credentials");   //Modify with Toaster Messages
    // }
    if(loginForm.emailId === "HR" && loginForm.password === "HR") {

      //Authentication the User to access the components through authguard
      this.service.setLoginStatus();
      this.router.navigate(['showemps']);
    } else {
      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {
        
        //Authentication the User to access the components through authguard
        this.service.setLoginStatus();
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
      }
    }

  }
}