import { Component, OnInit, ɵINPUT_SIGNAL_BRAND_WRITE_TYPE } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})

export class RegisterComponent implements OnInit {
  employee: any;
  countries: any;

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
    this.employee = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: ''
    }
  }
  ngOnInit() {
    this.service.getAllContries().subscribe((data: any) => {
      console.log(data);
      this.countries = data;
    });
  }
  
  submit(registerForm: any): void {
    console.log(registerForm.value);
}

 


  // submit(){
  //   console.log(this.employee);
  // }


}