import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  id: number;
  name: string;
  age: number;
  hobbies:any;
  address: any;


  
  constructor() {
    // alert('Constructor Invoked...');
    this.id=101;
    this.name='sahithya';
    this.age=24;
    this.hobbies=['Shuttle','TV', 'Crafts','Music','cooking']
    this.address={Hno: 1-8-35,StreetNo:'Girnigadda',city:'Jangaon', state:'Telangana' }


  }

  ngOnInit() {
    // alert('ngOnInit Invoked...');
  }
}
