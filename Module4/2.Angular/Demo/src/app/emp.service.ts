import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
  }

  getAllContries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  setLoginStatus() {
    this.isUserLoggedIn = true;
  }
  setUserLoggedIn() {
    this.isUserLoggedIn = true;
  }
  setUserLoggedOut() {
    this.isUserLoggedIn = false;
  }
  getLoginStatus(): boolean {
    return this.isUserLoggedIn;
  }
}