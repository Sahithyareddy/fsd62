import { Component } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent {

  employees: any; 

  //Date Format: mm-DD-YYYY
  constructor() {
    this.employees = [
      {empId:101, empName:'Sahithya', salary:1212.12, gender:'Female',   country:'IND', doj:'06-13-2018'},
      {empId:102, empName:'Shruthika',  salary:2323.23, gender:'Female',   country:'CHI', doj:'07-14-2017'},
      {empId:103, empName:'Manasa', salary:3434.34, gender:'Female', country:'USA', doj:'08-15-2016'},
      {empId:104, empName:'Vaishnavi',  salary:4545.45, gender:'Female', country:'FRA', doj:'09-16-2015'},
      {empId:105, empName:'Deekshitha',  salary:5656.56, gender:'Female',   country:'NEP', doj:'10-17-2014'},
      {empId:106, empName:'Krupa',  salary:5656.56, gender:'Female',   country:'NEP', doj:'11-17-2014'}
    ];

  }
  

}