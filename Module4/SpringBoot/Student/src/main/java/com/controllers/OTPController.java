package com.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.OTPService;

@RestController
public class OTPController {

    @Autowired
    private OTPService otpService;

    @PostMapping("/sendOTP")
    public ResponseEntity<String> sendOTP(@RequestBody String email) {
        try {
            String otp = otpService.generateOTP();
            otpService.sendOTPEmail(email, otp);
            return ResponseEntity.ok("OTP sent successfully to: " + email);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send OTP");
        }
    }
}


