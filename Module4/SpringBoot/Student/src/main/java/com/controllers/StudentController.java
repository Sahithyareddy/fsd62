package com.controllers;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dao.EmailService;
import com.dao.OTPService;
import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {

    @Autowired
    private StudentDao stdDao;

    @Autowired
    private EmailService emailService;

    @PostMapping("stdLogin")
    public ResponseEntity<?> stdLogin(@RequestBody Student credentials) {
        String emailId = credentials.getEmailId();
        String enteredPassword = credentials.getPassword();
        
        Student student = stdDao.getStudentByEmail(emailId);
        
        if (student == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid email or password");
        }
        
        String storedPassword = student.getPassword();
        
        // Decrypt the stored password using BCrypt
        boolean passwordMatch = BCrypt.checkpw(enteredPassword, storedPassword);
        
        if (passwordMatch) {
            return ResponseEntity.ok(student);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid email or password");
        }
    }

    @GetMapping("getAllStudents")
    public List<Student> getAllStudents() {
        return stdDao.getAllStudents();
    }

    @GetMapping("getStudentById/{id}")
    public Student getStudentById(@PathVariable("id") int stdId) {
        return stdDao.getStudentById(stdId);
    }

    @GetMapping("getStudentByName/{name}")
    public List<Student> getStudentByName(@PathVariable("name") String stdName) {
        return stdDao.getStudentByName(stdName);
    }

    @PostMapping("addStudent")
    public ResponseEntity<?> addStudent(@RequestBody Student std) {
        try {
            Student savedStudent = stdDao.addStudent(std);
            // Send confirmation email
            emailService.sendEmail(std.getEmailId());
            return ResponseEntity.ok(savedStudent);
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists");
        }
    }
    
    @PostMapping("sendConfirmationEmail")
    public ResponseEntity<String> sendConfirmationEmail(@RequestBody String email) {
        try {
            // Send confirmation email
            emailService.sendEmail(email);
            return ResponseEntity.ok("Confirmation email sent successfully to: " + email);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send confirmation email");
        }
    }
    
    @PutMapping("updateStudent")
    public Student updateStudent(@RequestBody Student std) {
        return stdDao.updateStudent(std);
    }

    @DeleteMapping("deleteStudentById/{id}")
    public String deleteStudentById(@PathVariable("id") int stdId) {
        stdDao.deleteStudentById(stdId);
        return "Student Record Deleted Successfully!!!";
    }
}

